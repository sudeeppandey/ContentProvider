package pandey.sudeep.contentprovider;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

@Dao
public interface MovieDAO {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertAllMovies(List<BestMovies> myMovies);

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertAllLeads(List<LeadCharacters> movieLeads);

    @Query("Select * from bestmovies")
    List<BestMovies> loadAllMovies();

    @Query("Select count(*) from bestmovies")
    int countRecords();

    @Query("Select * from bestmovies")
    Cursor loadAllMoviesForCursor();

    @Query("Select * from bestmovies where id=:id")
    Cursor loadOneMovieForCursor(int id);

    @Query("Select * from leadroles")
    List<LeadCharacters> loadAllLeads();

    @Query("Select * from leadroles")
    Cursor loadAllLeadsForCursor();

    @Query("Select * from leadroles where id=:id")
    Cursor loadOneActorForCursor(int id);

    @Update
    int updateMovie(BestMovies movie);

    @Update
    int updateLeadCharacter(LeadCharacters lead);

    @Query("Delete from bestmovies where id= :movieToDelete")
    int deleteMovie(int movieToDelete);

}
