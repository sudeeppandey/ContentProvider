package pandey.sudeep.contentprovider;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import java.util.List;

@Database(entities={BestMovies.class,LeadCharacters.class},version=1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase sInstance;

    @VisibleForTesting
    public static final String DATABASE_NAME = "sample-db";

    public abstract MovieDAO getMovieDAO();

    public static synchronized AppDatabase getInstance(Context context) {
        if (sInstance == null) {

            sInstance = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).build();
            sInstance.populateInitialData();
        }
        return sInstance;
    }
    private void populateInitialData(){
                if(sInstance.getMovieDAO().countRecords()==0) {
                    sInstance.beginTransaction();
                    try {
                        sInstance.getMovieDAO().insertAllMovies(DataGenerator.generateMovies());
                        sInstance.getMovieDAO().insertAllLeads(DataGenerator.generateLeadActors());
                        sInstance.setTransactionSuccessful();
                    } finally {
                        sInstance.endTransaction();
                    }
                }
        }

    /**
     * Build the database. {@link Builder#build()} only sets up the database configuration and
     * creates a new instance of the database.
     * The SQLite database is only created when it's accessed for the first time.
     */
   /* private static AppDatabase buildDatabase(final Context appContext,
                                             final AppExecutors executors) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        executors.diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                // Add a delay to simulate a long-running operation
                                // addDelay();
                                // Generate the data for pre-population
                                final AppDatabase database = AppDatabase.getInstance(appContext, executors);
                                final List<BestMovies> movies = DataGenerator.generateMovies();
                                final List<LeadCharacters> leads = DataGenerator.generateLeadActors();
                                //database.getMovieDAO().insertAllMovies(movies);
                                //database.getMovieDAO().insertAllLeads(leads);

                                database.runInTransaction(new Runnable() {
                                    @Override
                                    public void run() {
                                        database.getMovieDAO().insertAllMovies(movies);
                                        database.getMovieDAO().insertAllLeads(leads);
                                    }
                                });
                            }
                        });
                    }
                }).build();

    }*/
}

