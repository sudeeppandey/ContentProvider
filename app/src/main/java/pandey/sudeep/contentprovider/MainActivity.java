package pandey.sudeep.contentprovider;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;
    private LinearLayoutManager linearLayoutManager1;
    private LinearLayoutManager linearLayoutManager2;
    private MovieAdapter movieAdapter;
    private LeadsAdapter leadsAdapter;
    private Context context = this;
    private int movieCount;

    protected final String TAG = getClass().getSimpleName();

    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final AppExecutors executors = new AppExecutors();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                 database = AppDatabase.getInstance(context);
            }
        });



       // executors.diskIO().execute(new Runnable() {
         //   @Override
           // public void run() {
             //   movieCount = database.getMovieDAO().countRecords();
               // Log.d(TAG+"This is my log: ", Integer.toString(movieCount));

            //}
       // });


        Button button = (Button)findViewById(R.id.updateTable1);
        button.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          executors.diskIO().execute(new Runnable() {
                                              @Override
                                              public void run() {
                                                  movieCount = database.getMovieDAO().countRecords();
                                                  Log.d(TAG+"This is my log: ", Integer.toString(movieCount));
                                              }
                                          });

                                      }
                                  });
/*
        executors.diskIO().execute(new Runnable(){
            @Override
            public void run(){
                movieAdapter = new MovieAdapter(database.getMovieDAO().loadAllMovies());
                leadsAdapter = new LeadsAdapter(database.getMovieDAO().loadAllLeads());
                executors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView1 = findViewById(R.id.recyclerView);
                        recyclerView1.setHasFixedSize(true);
                        linearLayoutManager1 = new LinearLayoutManager(context);
                        recyclerView1.setLayoutManager(linearLayoutManager1);

                        recyclerView2 = findViewById(R.id.recyclerView2);
                        recyclerView2.setHasFixedSize(true);
                        linearLayoutManager2 = new LinearLayoutManager(context);
                        recyclerView2.setLayoutManager(linearLayoutManager2);

                        recyclerView1.setAdapter(movieAdapter);
                        recyclerView2.setAdapter(leadsAdapter);
                    }
                });
            }
        });*/
    }
}
