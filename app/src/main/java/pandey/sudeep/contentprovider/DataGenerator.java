package pandey.sudeep.contentprovider;

import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

    public static List<BestMovies> generateMovies(){

        List<BestMovies> movielist = new ArrayList<BestMovies>();

            movielist.add(new BestMovies(1,"Pirates Of The Carribean","Action"));
            movielist.add(new BestMovies(2,"Fast & Furious Series","Action"));
            movielist.add(new BestMovies(3,"James Bond Series","Action"));
            movielist.add(new BestMovies(4,"Sherlock Holmes","Suspense"));
            movielist.add(new BestMovies(5,"The Dark Knight","Thriller/Action"));
            movielist.add(new BestMovies(6,"Justice League","Comics Action"));
            movielist.add(new BestMovies(7,"Avengers","Comics Action"));
            movielist.add(new BestMovies(8,"Transformers Series","Science Fiction"));
            movielist.add(new BestMovies(9,"Mission Impossible Series","Action/Spy"));
            movielist.add(new BestMovies(10,"Troy","Action/Adventure"));

            return movielist;

    }

    public static List<LeadCharacters> generateLeadActors(){

        List<LeadCharacters> leadHeroes = new ArrayList<LeadCharacters>();
            leadHeroes.add(new LeadCharacters(10,"Brad Pitt"));
            leadHeroes.add(new LeadCharacters(8,"Optimus Prime"));
            leadHeroes.add(new LeadCharacters(3,"Daniel Craig"));
            leadHeroes.add(new LeadCharacters(5,"Heath Ledger"));
            leadHeroes.add(new LeadCharacters(1,"Johnny Depp"));
            leadHeroes.add(new LeadCharacters(6,"Ben Affleck"));
            leadHeroes.add(new LeadCharacters(4,"Robert Downey Jr."));
            leadHeroes.add(new LeadCharacters(2,"Vin Diesel"));
            leadHeroes.add(new LeadCharacters(7,"Marvel Folks"));
            leadHeroes.add(new LeadCharacters(9,"Tom Cruise"));

            return leadHeroes;
    }
}
