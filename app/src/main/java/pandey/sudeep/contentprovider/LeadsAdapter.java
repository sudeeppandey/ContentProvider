package pandey.sudeep.contentprovider;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.ViewHolder> {

    private List<LeadCharacters> leads;

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;

        public ViewHolder(CardView cv){
            super(cv);
            this.cardView=cv;
        }
    }

    public LeadsAdapter(List<LeadCharacters> _leads){
        this.leads=_leads;
    }

    @Override
    public int getItemCount(){
        return leads.size();
    }

    @Override
    public LeadsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        LeadsAdapter.ViewHolder vh = new LeadsAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(LeadsAdapter.ViewHolder holder, final int position) {

        final CardView cardView = holder.cardView;
        TextView textView1 = (TextView)cardView.findViewById(R.id.textView);
        textView1.setText(Integer.toString(leads.get(position).getId()));
        TextView textView2 = (TextView)cardView.findViewById(R.id.textView2);
        textView2.setText(Integer.toString(leads.get(position).getMovieId()));
        TextView textView3 = (TextView)cardView.findViewById(R.id.textView3);
        textView3.setText(leads.get(position).getLead());

    }
}
