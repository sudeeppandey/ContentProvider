package pandey.sudeep.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class AppContentProvider extends ContentProvider {

    public static final String AUTHORITY = "pandey.sudeep.contentprovider";

    public static final Uri URI_MOVIES = Uri.parse(
            "content://" + AUTHORITY + "/" + "bestmovies");

    public static final Uri URI_LEADACTORS = Uri.parse(
            "content://" + AUTHORITY + "/" + "leadroles");

    private static final int CODE_MOVIES_DIR = 1;
    private static final int CODE_MOVIES_ITEM = 2;
    private static final int CODE_LEADS_DIR = 3;
    private static final int CODE_LEADS_ITEM = 4;

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        MATCHER.addURI(AUTHORITY, "bestmovies", CODE_MOVIES_DIR);
        MATCHER.addURI(AUTHORITY, "bestmovies" + "/*", CODE_MOVIES_ITEM);
        MATCHER.addURI(AUTHORITY,"leadroles",CODE_LEADS_DIR);
        MATCHER.addURI(AUTHORITY,"leadroles"+"/*",CODE_LEADS_ITEM);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder){

        final int code = MATCHER.match(uri);
        if(code==CODE_MOVIES_DIR||code==CODE_MOVIES_ITEM||code==CODE_LEADS_DIR||code==CODE_LEADS_ITEM){
            final Context context = getContext();
            if(context==null){
                return null;
            }
            AppExecutors executors = new AppExecutors();
            final MovieDAO dao = AppDatabase.getInstance(context).getMovieDAO();

            Cursor cursor=null;
            if(code==CODE_MOVIES_DIR){
                cursor = dao.loadAllMoviesForCursor();
            }else if(code==CODE_MOVIES_ITEM){
                cursor = dao.loadOneMovieForCursor((int)ContentUris.parseId(uri));
            }else if(code==CODE_LEADS_DIR){
                cursor = dao.loadAllLeadsForCursor();
            }else if(code==CODE_LEADS_ITEM){
                cursor = dao.loadOneActorForCursor((int)ContentUris.parseId(uri));
            }
            return cursor;
        }else{
            throw new IllegalArgumentException("Unknown URI: "+uri);
        }

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (MATCHER.match(uri)) {
            case CODE_MOVIES_DIR:
                return "vnd.android.cursor.dir/" + AUTHORITY + "." + "bestmovies";
            case CODE_MOVIES_ITEM:
                return "vnd.android.cursor.item/" + AUTHORITY + "." + "bestmovies";
            case CODE_LEADS_DIR:
                return "vnd.android.cursor.dir/" + AUTHORITY +"." + "leadroles";
            case CODE_LEADS_ITEM:
                return "vnd.android.cursor.item/" + AUTHORITY + "." + "leadroles";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values){
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        switch (MATCHER.match(uri)) {
            case CODE_MOVIES_DIR:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case CODE_LEADS_DIR:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case CODE_MOVIES_ITEM:
                final Context context = getContext();
                if (context == null) {
                    return 0;
                }
                final int count = AppDatabase.getInstance(context).getMovieDAO()
                        .deleteMovie((int)ContentUris.parseId(uri));
                context.getContentResolver().notifyChange(uri, null);
                return count;
            case CODE_LEADS_ITEM:
                return 0;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
                      @Nullable String[] selectionArgs){
        final Context context = getContext();
        switch(MATCHER.match(uri)){
            case CODE_MOVIES_DIR:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case CODE_LEADS_DIR:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case CODE_MOVIES_ITEM:
                if(context==null){
                    return 0;
                }
                BestMovies movie = new BestMovies((int)ContentUris.parseId(uri),(String)values.get("movieTitle"),(String)values.get("genre"));
                int count = AppDatabase.getInstance(context).getMovieDAO().updateMovie(movie);
                return count;
            case CODE_LEADS_ITEM:
                if(context==null){
                    return 0;
                }
                LeadCharacters lead = new LeadCharacters((int)values.get("movieId"),(String)values.get("lead"));
                int cnt = AppDatabase.getInstance(context).getMovieDAO().updateLeadCharacter(lead);
                return cnt;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }
}
