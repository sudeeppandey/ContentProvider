package pandey.sudeep.contentprovider;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName="leadroles",foreignKeys={@ForeignKey(entity=BestMovies.class,parentColumns="id",childColumns="movieId",onDelete= ForeignKey.CASCADE)},
        indices={@Index(value="lead")})
public class LeadCharacters {

    @PrimaryKey(autoGenerate = true)
    private int id; //shall we use long type instead?
    private int movieId;
    private String lead;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getLead() {
        return lead;
    }

    public void setLead(String lead) {
        this.lead = lead;
    }

    public LeadCharacters(){ }

    public LeadCharacters(int _movieId, String leadCharacter){
        this.movieId=_movieId;
        this.lead=leadCharacter;
    }
}
